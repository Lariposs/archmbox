# Copyright (C) 2002 Alessandro Dotti Contra <adotti@users.sourceforge.net>
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

AC_INIT
AM_INIT_AUTOMAKE(archmbox,4.10.0)
AC_PROG_INSTALL

dnl Configurable options

AC_ARG_VAR(ARCHMBOX_TMP_DIR,[Working directory for temporary mailboxes (default: /tmp)])

dnl Test the required binaries

AC_PATH_PROG(PERL,perl,"no")
test $PERL == "no" && AC_MSG_ERROR(perl is not installed!)
AC_PATH_PROG(RM,rm,"no")
test $RM == "no" && AC_MSG_ERROR(rm is not installed!)
AC_PATH_PROG(CAT,cat,"no")
test $CAT == "no" && AC_MSG_ERROR(cat is not installed!)
AC_PATH_PROG(CP,cp,"no")
test $CP == "no" && AC_MSG_ERROR(cp is not installed!)
AC_PATH_PROG(GZIP,gzip,"no")
test $GZIP == "no" && AC_MSG_ERROR(gzip is not installed!)
AC_PATH_PROG(GUNZIP,gunzip,"no")
test $GUNZIP == "no" && AC_MSG_ERROR(gzip is not installed!)
AC_PATH_PROG(WC,wc,"no")
test $WC == "no" && AC_MSG_ERROR(wc is not installed!)

dnl Test for fuser/fstat/lsof. We can use any of them

AC_PATH_PROGS(FUSER,[fuser fstat lsof],"no")
test $FUSER == "no" && AC_MSG_ERROR(none of fuser, fstat or lsof is installed!)

dnl Test the optional binaries

AC_PATH_PROG(BZIP2,bzip2,"no")
test $BZIP2 == "no" && AC_MSG_WARN([
	bzip2 is not installed. bzip compression won't be available.
])
AC_PATH_PROG(BUNZIP2,bunzip2,"no")
test $BUNZIP2 == "no" && AC_MSG_WARN([
	bunzip2 is not installed. bzip compression won't be available.
])

dnl Test the mbx mailboxes converter

AC_PATH_PROGS(MBXCVT,[mbxcvt mailutil],"no")
test $MBXCVT == "no" && AC_MSG_WARN([
	mbx mailboxes convert was not found. mbx mailboxes support disabled.
])

AC_OUTPUT(Makefile src/archmbox.pl)
